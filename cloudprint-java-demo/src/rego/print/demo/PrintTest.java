package rego.print.demo;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import rego.print.api.PrintApis;
import rego.print.exception.ServerErrorException;

@SuppressWarnings("restriction")
public class PrintTest {
	/**
	 * 打印服务器URL
	 */
	private static final String url = "http://printsrv.easyprt.com";
    // 设置APP KEY
    private static final String key = "your access key";
    // 设置APP SECRET
    private static final String secret = "your access secret";
    //回掉地址，如果打印成功
    private static final String callback = "your callback server address";
    //打印机ID号
    private static final String printId = "your printer id";
    
    /**
     * 回掉地址接收到的bussinessid，用于进行对比
     */
    private static String resultUrl;
    /**
     * 云打印服务器库
     */
    private static PrintApis cloudPrint;
    private static final Object waitObject = new Object();
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("cloud print demo");
        try {
        	cloudPrint = new PrintApis(url, key, secret);
        	new Thread(new PrintThread(printId, callback)).start();
            start(8888);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	/**
	 * 发送打印数据线程
	 * @author wlp
	 *
	 */
	public static class PrintThread implements Runnable {
		private String printId;
		private String callBack;
		public PrintThread(String printId, String callBack) {
			this.printId = printId;
			this.callBack = callBack;
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			System.out.println("Send data thread running...");
			String bussId;
			int num = 1;
			for(;;) {
				try {
					synchronized (waitObject) {
						String status = cloudPrint.GetPrinterStatus(printId);
						if("OK".equalsIgnoreCase(status)) {
							/**
							 * 发送实际打印数据，可以对当前打印任务监控，实现一单一控，以便及时掌握打印情况
							 */
							bussId = cloudPrint.PrintString("cloud print test ("+num+")\r\n", printId, callBack, "");
							System.out.println("--------------------------------------");
							System.out.println("Print job ("+bussId+") been send");
							if(bussId==null || bussId=="") {
								System.out.println("Print test error, code is:" + cloudPrint.getErrCode());
							} else  {
								long dw = System.currentTimeMillis();
								waitObject.wait(5000); //等待最大延时
								dw = System.currentTimeMillis()-dw;
								if(bussId.equalsIgnoreCase(resultUrl)) {
									System.out.println("This print job complete, used time is:" + dw);
									num ++;
								}
								else
									System.out.println("Other print job complete, used time is:" + dw);
								
								if(dw>=5000) {
									System.out.println("This ticket overtime, we need query the printer status and delete the printer job");
									status = cloudPrint.GetPrinterStatus(printId);
									System.out.println("printer status is: " + status);
									boolean deleteJob = cloudPrint.CancelTask(bussId);
									System.out.println("Delete the printer job("+bussId+") is "+ deleteJob);
								}
							}
						}
						else {
							System.out.println("The printer " + printId + " status error("+ status +")");
						}						
					}
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ServerErrorException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * 启动回掉HTTP服务，用于接收打印任务完成回掉信息
	 * @param port 回掉地址
	 * @throws IOException
	 */
    public static void start(Integer port) throws IOException {
        InetSocketAddress address = new InetSocketAddress(port);
        HttpServer server = HttpServer.create(address, 0);
        if (server != null) {
            server.createContext("/callback", new CallbackHandler());
            server.setExecutor(null);
            server.start();
        }
    }
    
    /**
     * 模拟服务器接收回掉数据
     * @author wlp
     *
     */
    static class CallbackHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            int code = 200;
            String response = "ok";
            try {
            	synchronized (waitObject) {
	            	String strId, strBussinessId;
	            	String reqStr = t.getRequestURI().toString();
	                System.out.println("Complete print job request:"+reqStr);
	                reqStr = reqStr.substring(reqStr.indexOf("?")+1, reqStr.length());
	                String []paras = reqStr.split("&");
	            	strId = paras[0].split("=")[1];
	            	strBussinessId = paras[1].split("=")[1];
	            	System.out.println("PrintId:"+strId + "\t\tBussinessId:"+strBussinessId);
	            	resultUrl = strBussinessId;
	            	waitObject.notifyAll();
            	}
            } catch (Exception e) {
                e.printStackTrace();
                code = 500;
                response = e.getMessage();
            } finally {
                Map<String, String> responseMap = new HashMap<String, String>();
                responseMap.put("message", response);
                responseMap.put("code", String.valueOf(code));
                String resultJson = rego.print.utils.JacksonUtils.obj2json(responseMap);
                response(t, resultJson);
            }
        }
    }

    /**
     * 回复数据
     * @param t
     * @param responseMsg
     */
    private static void response(HttpExchange t, String responseMsg) {
        try {
            t.sendResponseHeaders(HttpURLConnection.HTTP_OK, responseMsg.getBytes().length);
            OutputStream os = t.getResponseBody();
            os.write(responseMsg.getBytes("UTF-8"));
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
