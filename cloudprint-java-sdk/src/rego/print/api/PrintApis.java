package rego.print.api;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import rego.print.beans.DataPrinterListBean;
import rego.print.beans.PrinterList;
import rego.print.beans.ServerBean;
import rego.print.beans.TaskList;
import rego.print.consts.consts;
import rego.print.exception.ServerErrorException;
import rego.print.utils.JacksonUtils;
import rego.print.utils.PrintUtils;

@SuppressWarnings("restriction")
public class PrintApis {
	/**
	 * 初始化云打印库
	 * @param serverUrl 云打印服务器的地址
	 * @param strAccessKey 厂家颁发给用户的key
	 * @param strAccessSecret 厂家办法给用户的secret
	 */
	public PrintApis(String serverUrl, String strAccessKey, String strAccessSecret) {
		super();
		this.serverUrl = serverUrl;
		this.strAccessKey = strAccessKey;
		this.strAccessSecret = strAccessSecret;
		this.token = "";
	}

	/**
	 * 获取客户端连接Token
	 * @return 非空就是服务器返回的Token
	 */
	private String GetToken() throws ServerErrorException {
		String strToken = "";
		errCode = consts.IO_ERROR;
		
		try {
			String totUrl = serverUrl + "/api/getAccessToken?";
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("accessKey", strAccessKey);
			data.put("accessSecret", strAccessSecret);
			String strRet = PrintUtils.requestPost(data, totUrl, "");
			ServerBean<?> serverBean = JacksonUtils.json2pojo(strRet, ServerBean.class);
			errCode = serverBean.getCode();
			if (serverBean.getCode() == 0)
				strToken = (String) serverBean.getData();
		}  catch (ServerErrorException e) {
			throw e;
		} 
		return strToken;
	}

	/**
	 * 获取打印机状态信息
	 * 
	 * @param printerId
	 *            打印机Id号
	 * @return 打印机状态值
	 */
	public String GetPrinterStatus(String printerId) throws ServerErrorException {
		String strStatus = "";
		errCode = consts.IO_ERROR;
		try {
			if(token=="") {
				token = GetToken();
				if(token=="")
					throw new ServerErrorException("获取服务器token失败");
			}
			
			String totUrl = serverUrl + "/api/queryPrinterList?";
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("printerId", printerId);
			data.put("page", "1");
			data.put("pageSize", "1");
			String strRet = PrintUtils.requestPost(data, totUrl, token);
			ServerBean<?> serverBean = JacksonUtils.json2pojo(strRet, ServerBean.class);
			errCode = serverBean.getCode();
			if (serverBean.getCode() == 0) {
				DataPrinterListBean mList = JacksonUtils.getInstance().convertValue(serverBean.getData(),
						DataPrinterListBean.class);
				List<PrinterList> dlb = mList.getList();
				if (dlb.size() > 0)
					strStatus = dlb.get(0).getStatus();
			} else if (serverBean.getCode() == consts.TOKEN_EXPIRE ) {
				token = "";
				strStatus = GetPrinterStatus(printerId);
			}
		} catch(ServerErrorException e) {
			
		}
		return strStatus;
	}

	/**
	 * 发送打印数据给打印机
	 * @param textSend 文本打印数据
	 * @param printerId 打印机ID
	 * @param strCallback 回掉地址，打印任务成功后服务器会调用这个地址发送消息
	 * @param bussId 任务id，可以为空
	 * @return 非空值表示打印成功，并且是有效的任务id
	 */
	public String PrintString(String textSend, String strId, String strCallback, String bussId) throws ServerErrorException {
		//
		String strRetval = "";
		String totUrl = serverUrl + "/api/addPrintTask?";
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("printerId", strId);
		if(bussId==null || bussId=="")
			bussId = PrintUtils.MakeBussid();
		data.put("businessId", bussId);
		
		errCode = consts.IO_ERROR;
		try {
			if(token=="") {
				token = GetToken();
				if(token=="")
					throw new ServerErrorException("获取服务器token失败");
			}
			
			byte[] btText = textSend.getBytes("gb2312");
			String enCode = Base64.encode(btText);
			enCode = enCode.replaceAll("[\r\n]", "");
//			System.out.println("打印数据:" + enCode);
			data.put("data", enCode);
			data.put("notifyUrl", strCallback);
			String strRet = PrintUtils.PostData(data, totUrl, token);
			ServerBean<?> serverBean = JacksonUtils.json2pojo(strRet, ServerBean.class);
			errCode = serverBean.getCode();
			if (serverBean.getCode() == 0) {
				strRetval = (String) serverBean.getData();
				strRetval = bussId;
			} else if (serverBean.getCode() == consts.TOKEN_EXPIRE ) {
				token = "";
				strRetval = PrintString(textSend, strId, strCallback, bussId);
			}
		} catch (UnsupportedEncodingException e1) {
			throw new ServerErrorException(e1.getMessage());
		} catch (ServerErrorException se) {
			throw se;
		}
		return strRetval;
	}

	/**
	 * 发送命令数据给打印机
	 * @param btText 命令形式的打印数据
	 * @param printerId 打印机ID
	 * @param strCallback 回掉地址，打印任务成功后服务器会调用这个地址发送消息
	 * @param bussId 任务id，可以为空
	 * @return 非空值表示打印成功，并且是有效的任务id
	 * @throws ServerErrorException
	 */
	public String PrintString(byte[] btText, String printerId, String strCallback, String bussId) throws ServerErrorException {
		//
		String strRetval = "";
		String totUrl = serverUrl + "/api/addPrintTask?";
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("printerId", printerId);
		if(bussId==null || bussId=="")
			bussId = PrintUtils.MakeBussid();
		data.put("businessId", bussId);
		
		errCode = consts.IO_ERROR;
		try {
			if(token=="") {
				token = GetToken();
				if(token=="")
					throw new ServerErrorException("获取服务器token失败");
			}
			String enCode = Base64.encode(btText);
			enCode = enCode.replaceAll("[\r\n]", "");
			data.put("data", enCode);
			data.put("notifyUrl", strCallback);
			String strRet = PrintUtils.PostData(data, totUrl, token);
			ServerBean<?> serverBean = JacksonUtils.json2pojo(strRet, ServerBean.class);
			errCode = serverBean.getCode();
			if (serverBean.getCode() == 0) {
				strRetval = (String) serverBean.getData();
				strRetval = bussId;
			} else if (serverBean.getCode() == consts.TOKEN_EXPIRE ) {
				token = "";
				strRetval = PrintString(btText, printerId, strCallback, bussId);
			}
		} catch (ServerErrorException se) {
			throw se;
		}
		return strRetval;
	}

	/**
	 * 获取当前打印任务状态
	 * @param bussId 任务ID
	 * @return 任务状态字符串
	 */
	public String QueryTaskStatus(String bussId) {
		// 获取打印结果
		String strStatus = "";
		String totUrl = serverUrl + "/api/searchTask?";
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("businessId", bussId);
		errCode = consts.IO_ERROR;
		try {
			if(token=="") {
				token = GetToken();
				if(token=="")
					throw new ServerErrorException("获取服务器token失败");
			}
			
			String strRet = PrintUtils.requestPost(data, totUrl, token);
			ServerBean<?> serverBean = JacksonUtils.json2pojo(strRet, ServerBean.class);
			errCode = serverBean.getCode();
			if (serverBean.getCode() == 0) {
				TaskList mOneObj = JacksonUtils.getInstance().convertValue(serverBean.getData(), TaskList.class);
				strStatus = mOneObj.getStatus();
			} else if (serverBean.getCode() == consts.TOKEN_EXPIRE ) {
				token = "";
				strStatus = QueryTaskStatus(bussId);
			}
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return strStatus;
	}

	/**
	 * 撤销打印任务
	 * @param bussId 任务id
	 * @return true 成功 false 失败
	 */
	public boolean CancelTask(String bussId) {
		boolean bRet = false;
		String totUrl = serverUrl + "/api/cancelTask?";
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("businessId", bussId);
		errCode = consts.IO_ERROR;
		try {
			if(token=="") {
				token = GetToken();
				if(token=="")
					throw new ServerErrorException("获取服务器token失败");
			}
			
			String strRet = PrintUtils.requestPost(data, totUrl, token);
			ServerBean<?> serverBean = JacksonUtils.json2pojo(strRet, ServerBean.class);
			errCode = serverBean.getCode();
			if (serverBean.getCode() == 0) {
				bRet = true;
			} else if (serverBean.getCode() == consts.TOKEN_EXPIRE ) {
				token = "";
				bRet = CancelTask(bussId);
			}
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return bRet;
	}
	
	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public String getStrAccessKey() {
		return strAccessKey;
	}

	public void setStrAccessKey(String strAccessKey) {
		this.strAccessKey = strAccessKey;
	}

	public String getStrAccessSecret() {
		return strAccessSecret;
	}

	public void setStrAccessSecret(String strAccessSecret) {
		this.strAccessSecret = strAccessSecret;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	private String serverUrl;
	private String strAccessKey;
	private String strAccessSecret;
	private String token;
	private int errCode;
	public int getErrCode() {
		return errCode;
	}
}
