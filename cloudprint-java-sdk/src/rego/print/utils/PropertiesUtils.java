package rego.print.utils;

import java.util.ResourceBundle;

public class PropertiesUtils {
    /**
     * 从配置文件中获取相应的值
     * @param pKey 关键字段名称
     * @return 字段值
     */
    public static String getPropValueByKey(String pKey) {
        ResourceBundle rs = ResourceBundle.getBundle("config");
        return rs.getString(pKey);
    }
}
